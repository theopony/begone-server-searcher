﻿using BeGone.ServerSearcher.Api;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace BeGone.ServerSearcher
{
    public class Room
    {
        public static readonly String NetworkVersion = "7Cc4643";

        public delegate void PlayerFoundEvent(Room room, EventPlayerInfo player);
        public event PlayerFoundEvent OnPlayerFound;

        /// <summary>
        /// Full name as given by the Photon server.
        /// </summary>
        public String FullName { get; private set; }

        private int _playerCount = 0;
        /// <summary>
        /// Most recent player count as given by the Photon server.
        /// </summary>
        public int PlayerCount
        {
            get { return _playerCount; }
            set
            {
                _playerCount = value;
                this.LastUpdated = DateTime.Now;
            }
        }

        private List<EventPlayerInfo> _players = null;
        /// <summary>
        /// Get player information. If none has been received, an empty array will be returned.
        /// </summary>
        public EventPlayerInfo[] Players
        {
            get
            {
                if(_players != null)
                {
                    return _players.ToArray();
                }
                else
                {
                    return new EventPlayerInfo[0];
                }
            }
        }

        public Boolean HasPlayer(String playerName)
        {
            foreach(EventPlayerInfo e in this.Players)
            {
                if (e.Name.Equals(playerName))
                    return true;
            }

            return false;
        }

        /// <summary>
        /// The last time this room's player count was updated from the Photon server.
        /// </summary>
        public DateTime LastUpdated { get; private set; }

        /// <summary>
        /// The last time this room's player info was updated.
        /// </summary>
        public DateTime PlayersLastUpdated { get; private set; }

        /// <summary>
        /// Server info retrieved from NPlay's Api.
        /// </summary>
        public ServerInfo ServerInfo { get; private set; }

        /// <summary>
        /// Get the map name of the room from the full name.
        /// </summary>
        public String MapName
        {
            get
            {
                String[] fields = this.FullName.Split(new char[] { '_' });
                return fields[fields.Length - 1];
            }
        }

        /// <summary>
        /// Whether or not this is a Warehouse map.
        /// </summary>
        public Boolean IsWarehouse { get { return this.MapName.Equals("Warehouse"); } }

        /// <summary>
        /// Whether or not this is a Courtyard map.
        /// </summary>
        public Boolean IsCourtyard { get { return this.MapName.Equals("Courtyard"); } }

        /// <summary>
        /// Whether or not this is a Tower map.
        /// </summary>
        public Boolean IsTower { get { return this.MapName.Equals("Tower"); } }

        /// <summary>
        /// Whether or not this is a Timbertown map.
        /// </summary>
        public Boolean IsTimbertown { get { return this.MapName.Equals("Timbertown"); } }

        /// <summary>
        /// Whether or not this is a Crane map.
        /// </summary>
        public Boolean IsCrane { get { return this.MapName.Equals("Crane"); } }

        /// <summary>
        /// Whether or not this is a Pipeline map.
        /// </summary>
        public Boolean IsPipeline { get { return this.MapName.Equals("Pipeline"); } }

        /// <summary>
        /// Whether or not this room should be filtered (not visible) according to a filter state.
        /// </summary>
        /// <param name="filterState">Filter state</param>
        /// <returns>true if filtered, false if visible</returns>
        public Boolean IsFiltered(Controls.MapSelectionPanel.State filterState)
        {
            return !((this.IsWarehouse && filterState.Warehouse) ||
                    (this.IsCourtyard && filterState.Courtyard) ||
                    (this.IsTower && filterState.Tower) ||
                    (this.IsTimbertown && filterState.Timbertown) ||
                    (this.IsCrane && filterState.Crane) ||
                    (this.IsPipeline && filterState.Pipeline));
        }

        /// <summary>
        /// Get a ListViewItem that can be inserted into the Servers ListView.
        /// </summary>
        public ListViewItem ServersListViewItem
        {
            get
            {
                return new ListViewItem(new String[] { this.ServerInfo.Arena, this.ServerInfo.Name, this.PlayerCount.ToString(), this.FullName });
            }
        }

        private GameHandler _game = null;

        /// <summary>
        /// Construct a room from server info.
        /// </summary>
        /// <param name="info">Server info</param>
        public Room(ServerInfo info)
        {
            this.ServerInfo = info;
            this.FullName = GenerateFullName(info);
            this.PlayerCount = 0;
        }

        public Room(String roomName, int playerCount = 0)
        {
            this.FullName = roomName;
            this.PlayerCount = playerCount;
        }

        /// <summary>
        /// Generate a server's (room's) full name from the server info.
        /// </summary>
        /// <param name="info">Server info</param>
        /// <returns>Full name</returns>
        public static String GenerateFullName(ServerInfo info)
        {
            return String.Format("{0}_{1}_{2}_{3}",
                NetworkVersion, info.GameMode, info.Id, info.Arena);
        }

        /// <summary>
        /// Fetch the latest player info. Assumes this hasn't been done for awhile.
        /// </summary>
        public void FetchPlayers()
        {
            _game = new GameHandler(this);
            _game.OnPlayers += this.HandlePlayers;
            _game.Start();
        }

        private void HandlePlayers(EventPlayerInfo[] players)
        {
            Console.WriteLine("Handling players: " + players.Length);

            foreach(EventPlayerInfo player in players)
            {
                if(!this.HasPlayer(player.Name))
                {
                    // Player joined
                    if (this.OnPlayerFound != null)
                        this.OnPlayerFound(this, player);
                }
            }

            _players = new List<EventPlayerInfo>(players);

            var form = new Forms.ScoreboardForm(this);
            form.ShowDialog();
        }
    }
}
