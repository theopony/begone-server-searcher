﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using ExitGames.Client.Photon;
using ExitGames.Client.Photon.Lite;
using System.Collections;
using System.Threading;
using BeGone.ServerSearcher.Net;

namespace BeGone.ServerSearcher
{
    public class LobbyHandler : IPhotonPeerListener
    {
        public static readonly String[] LobbyIps = new String[]
        {
            "gs.chicago.nplay.com:5050",
            "gs.atlanta.nplay.com:5050",
            "31.186.250.88:5050"
        };

        public delegate void RoomListEvent(LobbyHandler lobby, Hashtable roomList);

        public LitePeer Peer { get; private set; }
        public String IpPort { get; private set; }
        public Thread Thread { get; private set; }

        public event RoomListEvent OnRoomList;

        /// <summary>
        /// You must first use this to connect to the server.
        /// </summary>
        /// <param name="ipPort"></param>
        /// <returns></returns>

        public LobbyHandler(String ipPort)
        {
            this.IpPort = ipPort;
        }

        public void Start()
        {
            Console.WriteLine("Starting LobbyHandler: " + this.IpPort);

            this.Peer = new LitePeer(this, ConnectionProtocol.Udp);
            this.Peer.ChannelCount = 8;
            this.Peer.DebugOut = DebugLevel.OFF;

            if (!this.Peer.Connect(this.IpPort, "BeGoneServer"))
            {
                Console.WriteLine("Failed to connect: " + this.IpPort);
            }

            this.Thread = new Thread(this.Run);
            this.Thread.Start();
        }

        /// <summary>
        /// You must use this after connecting to the server, to connect to the room you wish to join.
        /// </summary>
        /// <param name="roomName"></param>
        private void JoinWithLobby(string roomName)
        {
            Dictionary<byte, object> customOpParameters = new Dictionary<byte, object>();
            customOpParameters.Add(0xff, roomName);
            customOpParameters.Add(0xf2, "BeGone_lobby");
            this.Peer.OpCustom(0xff, customOpParameters, true);
        }

        public void DispatchNetworkEvent(PhotonEvent photonEvent, NetworkEvent e, bool isReliable, byte channel)
        {
            if ((this.Peer != null) && (e != null))
            {
                Hashtable info = e.GetInfo();
                this.Peer.OpRaiseEvent((byte)photonEvent, info, isReliable, channel);
            }
        }

        /// <summary>
        /// Needed by photon for some reason.  Must be called with a loop constantly in order for networking to work.
        /// </summary>
        public void Run()
        {
            while (true)
            {
                try
                {
                    this.Peer.Service();
                    Thread.Sleep(75);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Thread.Sleep(100);
                }
            }
        }

        public void OnEvent(EventData photonEvent)
        {
            switch (photonEvent.Code)
            {
                // Todo: Replace these values with their respective enum names
                case 0xfb:
                case 0xfc:
                    {
                        Hashtable roomList = (Hashtable)photonEvent[0xf5];
                        if (this.OnRoomList != null)
                            this.OnRoomList(this, roomList);

                        /*
                        IEnumerator enumerator = hashtable.Keys.GetEnumerator();
                        try
                        {
                            while (enumerator.MoveNext())
                            {
                                String current = (String)enumerator.Current;
                                lobby.UpdateRoomOnList(current, (String)hashtable[current]);
                            }
                        }
                        catch (Exception)
                        {
                        }
                        */
                        break;
                    }
            }

            /*
            //If you do this, then the list will no longer update.

            if(photonEvent.Code == 0xFC)
            {
                this.litePeer.Disconnect();//bla
            }
             */
        }

        public virtual void OnStatusChanged(StatusCode statusCode)
        {
            switch (statusCode)
            {
                case StatusCode.Connect:
                    Console.WriteLine("Joining the lobby.");
                    this.Peer.OpJoin("BeGone_lobby");
                    break;
                case StatusCode.ExceptionOnConnect:
                case StatusCode.Disconnect:
                case StatusCode.Exception:
                case StatusCode.TimeoutDisconnect:
                case StatusCode.DisconnectByServer:
                case StatusCode.DisconnectByServerUserLimit:
                case StatusCode.DisconnectByServerLogic:
                    Console.WriteLine("Unhandled Photon Error: " + statusCode);
                    break;

                default:
                    Console.WriteLine("Unhandled Photon Error: " + statusCode);
                    break;
            }
        }

        public void OnOperationResponse(OperationResponse or)
        {
        }

        public void DebugReturn(DebugLevel dl, String s)
        {
        }
    }
}
