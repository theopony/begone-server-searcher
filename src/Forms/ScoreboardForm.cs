﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BeGone.ServerSearcher.Forms
{
    public partial class ScoreboardForm : Form
    {
        public ScoreboardForm(Room room)
        {
            InitializeComponent();

            this.Text = room.ServerInfo.Name;
            this.ScoreboardPanel.Update(room);
        }
    }
}
