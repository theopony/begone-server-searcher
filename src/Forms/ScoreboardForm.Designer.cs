﻿namespace BeGone.ServerSearcher.Forms
{
    partial class ScoreboardForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ScoreboardPanel = new BeGone.ServerSearcher.Controls.ScoreboardPanel();
            this.SuspendLayout();
            // 
            // ScoreboardPanel
            // 
            this.ScoreboardPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ScoreboardPanel.Location = new System.Drawing.Point(0, 0);
            this.ScoreboardPanel.Name = "ScoreboardPanel";
            this.ScoreboardPanel.Size = new System.Drawing.Size(252, 262);
            this.ScoreboardPanel.TabIndex = 0;
            // 
            // ScoreboardForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(252, 262);
            this.Controls.Add(this.ScoreboardPanel);
            this.Name = "ScoreboardForm";
            this.Text = "ScoreboardForm";
            this.ResumeLayout(false);

        }

        #endregion

        private Controls.ScoreboardPanel ScoreboardPanel;
    }
}