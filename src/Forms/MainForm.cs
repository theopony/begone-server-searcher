﻿using BeGone.ServerSearcher.Api;
using BeGone.ServerSearcher.Net;
using RestSharp;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace BeGone.ServerSearcher.Forms
{
    public partial class MainForm : Form
    {
        private List<LobbyHandler> _lobbies = new List<LobbyHandler>();
        private Dictionary<String, int> _playerCounts = new Dictionary<String, int>();
        private Dictionary<String, Room> _rooms = new Dictionary<String, Room>();

        private Object _serverListLock = new Object();

        public Boolean IsFetchingServers { get; private set; }

        public Controls.MapSelectionPanel.State MapFilter { get; private set; }

        /// <summary>
        /// Total player count.
        /// </summary>
        public int TotalPlayerCount { get; private set; }

        /// <summary>
        /// Visible player count.
        /// </summary>
        public int VisiblePlayerCount { get; private set; }

        public MainForm()
        {
            this.InitializeComponent();
            this.MapFilter = this.ServersMapSelectionPanel.GenerateState();

            //this.InitializeServers();
        }

        private void InitializeServers()
        {
            NPlay nplay = new NPlay();
            nplay.OnServerInfo += this.HandleServerInfo;

            this.IsFetchingServers = true;
            Console.WriteLine("Fetching server info");
            this.StartStatusProgressBar();
            nplay.FetchServerInfo();
        }

        private void InitializeLobbies()
        {
            Console.WriteLine("Initializing lobbies...");

            foreach(var ip in LobbyHandler.LobbyIps)
            {
                LobbyHandler lobby = new LobbyHandler(ip);
                lobby.Start();
                lobby.OnRoomList += this.HandleRoomList;
                _lobbies.Add(lobby);
            }
        }

        /// <summary>
        /// Whether or not a room should be filtered from the server list.
        /// </summary>
        /// <param name="room">Room</param>
        /// <returns>true if filter, false if not</returns>
        private Boolean IsFiltered(Room room)
        {
            return room.IsFiltered(this.MapFilter) || room.PlayerCount == 0;
        }

        private void HandleServerInfo(IRestResponse<ServerInfoResponse> response)
        {
            if(response.ErrorMessage != null)
            {
                Console.WriteLine("Error fetching servers: " + response.ErrorMessage);
                this.IsFetchingServers = false;
                return;
            }

            List<ServerInfo> servers = response.Data.Servers;

            Console.WriteLine("Received info about " + servers.Count + " servers");
            List<ListViewItem> toAdd = new List<ListViewItem>();

            foreach(ServerInfo info in servers)
            {
                Room room = new Room(info);
                _rooms.Add(room.FullName, room);

                if(!this.IsFiltered(room))
                {
                    toAdd.Add(room.ServersListViewItem);
                }
            }

            this.ServersListView.Items.AddRange(toAdd.ToArray());

            this.StopStatusProgressBar();
            this.IsFetchingServers = false;

            // Start lobby handlers
            this.InitializeLobbies();
        }

        private void StartStatusProgressBar()
        {
            this.Invoke((MethodInvoker)delegate
            {
                this.StatusProgressBar.Style = ProgressBarStyle.Marquee;
            });
        }

        private void StopStatusProgressBar()
        {
            this.Invoke((MethodInvoker)delegate
            {
                this.StatusProgressBar.Style = ProgressBarStyle.Blocks;
            });
        }

        private void HandleRoomList(LobbyHandler lobby, Hashtable roomList)
        {
            Console.WriteLine("HandleRoomList");

            int totalPlayerCount = 0;

            lock (_serverListLock)
            {
                IEnumerator enumerator = roomList.Keys.GetEnumerator();
                while (enumerator.MoveNext())
                {
                    String roomName = (String)enumerator.Current;
                    int playerCount = int.Parse(roomList[roomName].ToString());

                    this.UpdatePlayerCount(roomName, playerCount);
                    totalPlayerCount += playerCount;
                }
            }

            this.RecalculatePlayerCounts();
        }

        /// <summary>
        /// Recalculate the total number of players.
        /// </summary>
        /// <returns></returns>
        private int RecalculatePlayerCounts()
        {
            int total = 0, visible = 0;

            lock(_playerCounts)
            {
                _playerCounts.Clear();

                lock(_rooms)
                {
                    Dictionary<String, Room>.Enumerator enumerator = _rooms.GetEnumerator();
                    while(enumerator.MoveNext())
                    {
                        Room room = enumerator.Current.Value;

                        if(_playerCounts.ContainsKey(room.ServerInfo.Ip))
                        {
                            _playerCounts[room.ServerInfo.Ip] += room.PlayerCount;
                        }
                        else
                        {
                            _playerCounts.Add(room.ServerInfo.Ip, room.PlayerCount);
                        }

                        if (!this.IsFiltered(room))
                            visible += room.PlayerCount;
                    }
                }

                Dictionary<String, int>.Enumerator countsEnumerator = _playerCounts.GetEnumerator();
                while (countsEnumerator.MoveNext())
                {
                    total += countsEnumerator.Current.Value;
                }
            }

            this.TotalPlayerCount = total;
            this.VisiblePlayerCount = visible;

            this.UpdateTotalPlayerCountLabel();
            this.UpdateVisiblePlayerCountLabel();

            return total;
        }

        public void UpdateTotalPlayerCountLabel()
        {
            this.TotalPlayerCountStatusLabel.Text = "Total Players: " + this.TotalPlayerCount;
        }

        public void UpdateVisiblePlayerCountLabel()
        {
            this.PlayerCountStatusLabel.Text = "Players: " + this.VisiblePlayerCount;
        }

        private void UpdatePlayerCount(String roomName, int playerCount)
        {
            Room room;
            if (!_rooms.TryGetValue(roomName, out room))
            {
                Console.WriteLine("Couldn't find room to update player count for: " + roomName);
                return;
            }

            room.PlayerCount = playerCount;

            ListViewItem existingItem = null;
            foreach(ListViewItem item in this.ServersListView.Items)
            {
                if(item.SubItems[3].Text.Equals(room.FullName))
                {
                    existingItem = item;
                    break;
                }
            }

            if (existingItem != null)
            {
                if (!this.IsFiltered(room))
                    // Update player count
                    existingItem.SubItems[2].Text = room.PlayerCount.ToString();
                else
                    this.ServersListView.Items.Remove(existingItem);
            }
            else if (!this.IsFiltered(room))
            {
                // Insert room with player count (if not filtered)
                this.ServersListView.Items.Add(room.ServersListViewItem);
            }
        }

        /// <summary>
        /// Update the server ListView according to a map filter.
        /// </summary>
        /// <param name="state">Map filter state</param>
        private void UpdateServerListView(Controls.MapSelectionPanel.State state)
        {
            lock(_serverListLock)
            {
                Extensions.DrawingControl.SuspendDrawing(this.ServersListView);

                List<ListViewItem> toAdd = new List<ListViewItem>();

                IEnumerator enumerator = _rooms.Values.GetEnumerator();
                while (enumerator.MoveNext())
                {
                    Room room = (Room)enumerator.Current;

                    ListViewItem existingItem = null;
                    foreach (ListViewItem item in this.ServersListView.Items)
                    {
                        if (item.SubItems[3].Text.Equals(room.FullName))
                        {
                            existingItem = item;
                            break;
                        }
                    }

                    if((room.IsFiltered(state) || room.PlayerCount == 0) && existingItem != null)
                    {
                        this.ServersListView.Items.Remove(existingItem);
                    }
                    else if (!room.IsFiltered(state) && room.PlayerCount > 0 && existingItem == null)
                    {
                        toAdd.Add(room.ServersListViewItem);
                    }
                }

                this.ServersListView.Items.AddRange(toAdd.ToArray());

                Extensions.DrawingControl.ResumeDrawing(this.ServersListView);
            }

            this.RecalculatePlayerCounts();
        }

        private void ExitMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            // Auto-close threads and stuff
            Environment.Exit(0);
        }

        private void ConsoleMenuItem_Click(object sender, EventArgs e)
        {
            if (!Debug.Console.IsVisible)
            {
                Debug.Console.ShowConsoleWindow();
            }
            else
            {
                Debug.Console.HideConsoleWindow();
            }
        }

        private void RefreshMenuItem_Click(object sender, EventArgs e)
        {
            this.InitializeServers();
        }

        private void ServersMapSelectionPanel_OnStateChanged(Controls.MapSelectionPanel.State state)
        {
            this.MapFilter = state;
            this.UpdateServerListView(state);
        }

        private void ServersListView_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (this.ServersListView.SelectedItems.Count > 0)
            {
                var item = this.ServersListView.SelectedItems[0];

                Room room = _rooms[item.SubItems[3].Text];
                if(room != null)
                {
                    room.FetchPlayers();
                }
            }
        }
    }
}
