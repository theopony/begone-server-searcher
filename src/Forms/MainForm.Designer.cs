﻿namespace BeGone.ServerSearcher.Forms
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.MainTabControl = new System.Windows.Forms.TabControl();
            this.ServersTabPage = new System.Windows.Forms.TabPage();
            this.ServersPanel = new System.Windows.Forms.Panel();
            this.ServersTopPanel = new System.Windows.Forms.Panel();
            this.PlayerTabPage = new System.Windows.Forms.TabPage();
            this.PlayersPanel = new System.Windows.Forms.Panel();
            this.MainPanel = new System.Windows.Forms.Panel();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.StatusProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.TotalPlayerCountStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.mainMenu1 = new System.Windows.Forms.MainMenu(this.components);
            this.FileMenuItem = new System.Windows.Forms.MenuItem();
            this.RefreshMenuItem = new System.Windows.Forms.MenuItem();
            this.FileSeparatorMenuItem1 = new System.Windows.Forms.MenuItem();
            this.ExitMenuItem = new System.Windows.Forms.MenuItem();
            this.ConsoleMenuItem = new System.Windows.Forms.MenuItem();
            this.HelpMenuItem = new System.Windows.Forms.MenuItem();
            this.AboutMenuItem = new System.Windows.Forms.MenuItem();
            this.StatusSeparator1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.StatusSeparator2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.PlayerCountStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.ServersListView = new BeGone.ServerSearcher.Controls.ListViewEx();
            this.hArena = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.hServer = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.hPlayers = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.hFullName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ServersMapSelectionPanel = new BeGone.ServerSearcher.Controls.MapSelectionPanel();
            this.MainTabControl.SuspendLayout();
            this.ServersTabPage.SuspendLayout();
            this.ServersPanel.SuspendLayout();
            this.ServersTopPanel.SuspendLayout();
            this.PlayerTabPage.SuspendLayout();
            this.MainPanel.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainTabControl
            // 
            this.MainTabControl.Controls.Add(this.ServersTabPage);
            this.MainTabControl.Controls.Add(this.PlayerTabPage);
            this.MainTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainTabControl.Location = new System.Drawing.Point(3, 3);
            this.MainTabControl.Name = "MainTabControl";
            this.MainTabControl.SelectedIndex = 0;
            this.MainTabControl.Size = new System.Drawing.Size(631, 284);
            this.MainTabControl.TabIndex = 2;
            // 
            // ServersTabPage
            // 
            this.ServersTabPage.Controls.Add(this.ServersPanel);
            this.ServersTabPage.Location = new System.Drawing.Point(4, 22);
            this.ServersTabPage.Name = "ServersTabPage";
            this.ServersTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.ServersTabPage.Size = new System.Drawing.Size(623, 258);
            this.ServersTabPage.TabIndex = 0;
            this.ServersTabPage.Text = "Servers";
            this.ServersTabPage.UseVisualStyleBackColor = true;
            // 
            // ServersPanel
            // 
            this.ServersPanel.Controls.Add(this.ServersListView);
            this.ServersPanel.Controls.Add(this.ServersTopPanel);
            this.ServersPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ServersPanel.Location = new System.Drawing.Point(3, 3);
            this.ServersPanel.Name = "ServersPanel";
            this.ServersPanel.Size = new System.Drawing.Size(617, 252);
            this.ServersPanel.TabIndex = 0;
            // 
            // ServersTopPanel
            // 
            this.ServersTopPanel.Controls.Add(this.ServersMapSelectionPanel);
            this.ServersTopPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.ServersTopPanel.Location = new System.Drawing.Point(0, 0);
            this.ServersTopPanel.Name = "ServersTopPanel";
            this.ServersTopPanel.Padding = new System.Windows.Forms.Padding(6);
            this.ServersTopPanel.Size = new System.Drawing.Size(617, 118);
            this.ServersTopPanel.TabIndex = 1;
            // 
            // PlayerTabPage
            // 
            this.PlayerTabPage.Controls.Add(this.PlayersPanel);
            this.PlayerTabPage.Location = new System.Drawing.Point(4, 22);
            this.PlayerTabPage.Name = "PlayerTabPage";
            this.PlayerTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.PlayerTabPage.Size = new System.Drawing.Size(623, 279);
            this.PlayerTabPage.TabIndex = 1;
            this.PlayerTabPage.Text = "Players";
            this.PlayerTabPage.UseVisualStyleBackColor = true;
            // 
            // PlayersPanel
            // 
            this.PlayersPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PlayersPanel.Location = new System.Drawing.Point(3, 3);
            this.PlayersPanel.Name = "PlayersPanel";
            this.PlayersPanel.Size = new System.Drawing.Size(617, 273);
            this.PlayersPanel.TabIndex = 0;
            // 
            // MainPanel
            // 
            this.MainPanel.Controls.Add(this.MainTabControl);
            this.MainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainPanel.Location = new System.Drawing.Point(0, 0);
            this.MainPanel.Name = "MainPanel";
            this.MainPanel.Padding = new System.Windows.Forms.Padding(3);
            this.MainPanel.Size = new System.Drawing.Size(637, 290);
            this.MainPanel.TabIndex = 3;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusProgressBar,
            this.StatusSeparator1,
            this.TotalPlayerCountStatusLabel,
            this.StatusSeparator2,
            this.PlayerCountStatusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 290);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(637, 24);
            this.statusStrip1.SizingGrip = false;
            this.statusStrip1.TabIndex = 3;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // StatusProgressBar
            // 
            this.StatusProgressBar.MarqueeAnimationSpeed = 300;
            this.StatusProgressBar.Name = "StatusProgressBar";
            this.StatusProgressBar.Size = new System.Drawing.Size(100, 18);
            // 
            // TotalPlayerCountStatusLabel
            // 
            this.TotalPlayerCountStatusLabel.AutoSize = false;
            this.TotalPlayerCountStatusLabel.Name = "TotalPlayerCountStatusLabel";
            this.TotalPlayerCountStatusLabel.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.TotalPlayerCountStatusLabel.Size = new System.Drawing.Size(120, 19);
            this.TotalPlayerCountStatusLabel.Text = "Total Players: 0";
            this.TotalPlayerCountStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.FileMenuItem,
            this.ConsoleMenuItem,
            this.HelpMenuItem});
            // 
            // FileMenuItem
            // 
            this.FileMenuItem.Index = 0;
            this.FileMenuItem.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.RefreshMenuItem,
            this.FileSeparatorMenuItem1,
            this.ExitMenuItem});
            this.FileMenuItem.Text = "File";
            // 
            // RefreshMenuItem
            // 
            this.RefreshMenuItem.Index = 0;
            this.RefreshMenuItem.Text = "Refresh";
            this.RefreshMenuItem.Click += new System.EventHandler(this.RefreshMenuItem_Click);
            // 
            // FileSeparatorMenuItem1
            // 
            this.FileSeparatorMenuItem1.Index = 1;
            this.FileSeparatorMenuItem1.Text = "-";
            // 
            // ExitMenuItem
            // 
            this.ExitMenuItem.Index = 2;
            this.ExitMenuItem.Text = "Exit";
            this.ExitMenuItem.Click += new System.EventHandler(this.ExitMenuItem_Click);
            // 
            // ConsoleMenuItem
            // 
            this.ConsoleMenuItem.Index = 1;
            this.ConsoleMenuItem.Text = "Console";
            this.ConsoleMenuItem.Click += new System.EventHandler(this.ConsoleMenuItem_Click);
            // 
            // HelpMenuItem
            // 
            this.HelpMenuItem.Index = 2;
            this.HelpMenuItem.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.AboutMenuItem});
            this.HelpMenuItem.Text = "Help";
            // 
            // AboutMenuItem
            // 
            this.AboutMenuItem.Index = 0;
            this.AboutMenuItem.Text = "About";
            // 
            // StatusSeparator1
            // 
            this.StatusSeparator1.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
            this.StatusSeparator1.BorderStyle = System.Windows.Forms.Border3DStyle.Etched;
            this.StatusSeparator1.Margin = new System.Windows.Forms.Padding(6, 3, 5, 2);
            this.StatusSeparator1.Name = "StatusSeparator1";
            this.StatusSeparator1.Size = new System.Drawing.Size(4, 19);
            // 
            // StatusSeparator2
            // 
            this.StatusSeparator2.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
            this.StatusSeparator2.BorderStyle = System.Windows.Forms.Border3DStyle.Etched;
            this.StatusSeparator2.Margin = new System.Windows.Forms.Padding(6, 3, 5, 2);
            this.StatusSeparator2.Name = "StatusSeparator2";
            this.StatusSeparator2.Size = new System.Drawing.Size(4, 19);
            // 
            // PlayerCountStatusLabel
            // 
            this.PlayerCountStatusLabel.Name = "PlayerCountStatusLabel";
            this.PlayerCountStatusLabel.Size = new System.Drawing.Size(56, 19);
            this.PlayerCountStatusLabel.Text = "Players: 0";
            // 
            // ServersListView
            // 
            this.ServersListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.hArena,
            this.hServer,
            this.hPlayers,
            this.hFullName});
            this.ServersListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ServersListView.FullRowSelect = true;
            this.ServersListView.GridLines = true;
            this.ServersListView.Location = new System.Drawing.Point(0, 118);
            this.ServersListView.MultiSelect = false;
            this.ServersListView.Name = "ServersListView";
            this.ServersListView.Size = new System.Drawing.Size(617, 134);
            this.ServersListView.TabIndex = 0;
            this.ServersListView.UseCompatibleStateImageBehavior = false;
            this.ServersListView.View = System.Windows.Forms.View.Details;
            this.ServersListView.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ServersListView_MouseDoubleClick);
            // 
            // hArena
            // 
            this.hArena.Text = "Arena";
            this.hArena.Width = 100;
            // 
            // hServer
            // 
            this.hServer.Text = "Server";
            this.hServer.Width = 150;
            // 
            // hPlayers
            // 
            this.hPlayers.Text = "Players";
            // 
            // hFullName
            // 
            this.hFullName.Text = "Full Name";
            this.hFullName.Width = 300;
            // 
            // ServersMapSelectionPanel
            // 
            this.ServersMapSelectionPanel.Dock = System.Windows.Forms.DockStyle.Right;
            this.ServersMapSelectionPanel.Location = new System.Drawing.Point(314, 6);
            this.ServersMapSelectionPanel.Name = "ServersMapSelectionPanel";
            this.ServersMapSelectionPanel.Size = new System.Drawing.Size(297, 106);
            this.ServersMapSelectionPanel.TabIndex = 0;
            this.ServersMapSelectionPanel.OnStateChanged += new BeGone.ServerSearcher.Controls.MapSelectionPanel.StateChangedCallback(this.ServersMapSelectionPanel_OnStateChanged);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(637, 314);
            this.Controls.Add(this.MainPanel);
            this.Controls.Add(this.statusStrip1);
            this.DoubleBuffered = true;
            this.Menu = this.mainMenu1;
            this.Name = "MainForm";
            this.Text = "BeGone Server Searcher";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.MainTabControl.ResumeLayout(false);
            this.ServersTabPage.ResumeLayout(false);
            this.ServersPanel.ResumeLayout(false);
            this.ServersTopPanel.ResumeLayout(false);
            this.PlayerTabPage.ResumeLayout(false);
            this.MainPanel.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl MainTabControl;
        private System.Windows.Forms.TabPage ServersTabPage;
        private System.Windows.Forms.TabPage PlayerTabPage;
        private System.Windows.Forms.Panel MainPanel;
        private System.Windows.Forms.Panel ServersPanel;
        private System.Windows.Forms.Panel PlayersPanel;
        private System.Windows.Forms.MainMenu mainMenu1;
        private System.Windows.Forms.MenuItem FileMenuItem;
        private System.Windows.Forms.MenuItem ExitMenuItem;
        private System.Windows.Forms.MenuItem HelpMenuItem;
        private System.Windows.Forms.MenuItem AboutMenuItem;
        private BeGone.ServerSearcher.Controls.ListViewEx ServersListView;
        private System.Windows.Forms.ColumnHeader hServer;
        private System.Windows.Forms.ColumnHeader hPlayers;
        private System.Windows.Forms.MenuItem ConsoleMenuItem;
        private System.Windows.Forms.MenuItem RefreshMenuItem;
        private System.Windows.Forms.MenuItem FileSeparatorMenuItem1;
        private System.Windows.Forms.Panel ServersTopPanel;
        private Controls.MapSelectionPanel ServersMapSelectionPanel;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel TotalPlayerCountStatusLabel;
        private System.Windows.Forms.ToolStripProgressBar StatusProgressBar;
        private System.Windows.Forms.ToolStripStatusLabel StatusSeparator1;
        private System.Windows.Forms.ToolStripStatusLabel StatusSeparator2;
        private System.Windows.Forms.ToolStripStatusLabel PlayerCountStatusLabel;
        private System.Windows.Forms.ColumnHeader hArena;
        private System.Windows.Forms.ColumnHeader hFullName;
    }
}

