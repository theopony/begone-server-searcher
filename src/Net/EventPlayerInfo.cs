﻿using System;
using System.Collections;
using BeGone.ServerSearcher.Net;

public class EventPlayerInfo : NetworkEvent
{
    public string AvatarUrl;
    public int Cash;
    public int ClanId;
    public string ClanPhotoUrl;
    public string ClanTitle;
    public int Deaths;
    public bool IsAlive;
    public bool IsRegistered;
    public int Kills;
    public string Name;
    public int TeamId;

    public EventPlayerInfo(int playerId, Hashtable info)
        : base(playerId, info)
    {
    }

    public override Hashtable GetInfo()
    {
        Hashtable hashtable = new Hashtable();
        return hashtable;
    }

    public override void SetInfo(Hashtable newInfo)
    {
        this.Name = (string)newInfo[(byte)0];
        this.TeamId = (int)newInfo[(byte)1];
        this.Kills = (int)newInfo[(byte)2];
        this.Deaths = (int)newInfo[(byte)3];
        this.IsAlive = (bool)newInfo[(byte)4];
        this.Cash = (int)newInfo[(byte)8];
        this.AvatarUrl = (string)newInfo[(byte)10];
        this.IsRegistered = (bool)newInfo[(byte)11];
        this.ClanId = (int)newInfo[(byte)12];
        this.ClanTitle = (string)newInfo[(byte)13];
        this.ClanPhotoUrl = (string)newInfo[(byte)14];
    }
}

