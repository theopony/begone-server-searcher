﻿using BeGone.ServerSearcher.Api;
using RestSharp;
using RestSharp.Deserializers;
using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;

namespace BeGone.ServerSearcher.Net
{
    public class NPlay
    {
        public static readonly String ApiBase = "ws2.nplay.com";
        public static readonly String ServerListPath = "nplay_getserverlist";

        public delegate void ServerInfoResponseCallback(IRestResponse<ServerInfoResponse> response);
        public event ServerInfoResponseCallback OnServerInfo;

        public void FetchServerInfo()
        {
            RestClient client = new RestClient("http://" + ApiBase);
            client.AddHandler("text/html", new JsonDeserializer());
            RestRequest request = new RestRequest(ServerListPath);

            client.ExecuteAsync<ServerInfoResponse>(request, response => {
                if (this.OnServerInfo != null)
                    this.OnServerInfo(response);
            });
        }

    }
}
