﻿using System;
using System.Collections;

namespace BeGone.ServerSearcher.Net
{
    public interface INetworkSerializable
    {
        Hashtable GetInfo();
        void SetInfo(Hashtable newInfo);
    }
}
