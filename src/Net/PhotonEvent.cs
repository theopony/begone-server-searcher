﻿namespace BeGone.ServerSearcher.Net
{
    public enum PhotonEvent
    {
        PlayerInfo = 0x65,
        RoundOver = 0x6f,
        RoundStart = 110,
        RoundStat = 0x7c,
        RoundStatus = 0x71,
        RoundWaiting = 0x72
    }
}
