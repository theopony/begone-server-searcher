﻿using System;
using System.Collections;

namespace BeGone.ServerSearcher.Net
{
    public abstract class NetworkEvent : INetworkSerializable
    {
        public int PlayerId { get; private set; }

        protected NetworkEvent(int playerId)
        {
            this.PlayerId = playerId;
        }

        protected NetworkEvent(int playerId, Hashtable info)
        {
            this.PlayerId = playerId;
            this.SetInfo(info);
        }

        public abstract Hashtable GetInfo();
        public abstract void SetInfo(Hashtable newInfo);
    }
}

