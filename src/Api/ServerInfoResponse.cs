﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeGone.ServerSearcher.Api
{
    public class ServerInfoResponse
    {
        public Boolean Successful
        {
            get { return this.Success == 1; }
        }

        public int Success { get; set; }
        public List<ServerInfo> Servers { get; set; }
    }
}
