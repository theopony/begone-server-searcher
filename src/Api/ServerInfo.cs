﻿using RestSharp.Deserializers;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeGone.ServerSearcher.Api
{
    /// <summary>
    /// Server information deserialized from JSON.
    /// </summary>
    public class ServerInfo
    {
        [DeserializeAs(Name = "_id")]
        public String Id { get; set; }

        public int OwnerUserId { get; set; }
        public String Name { get; set; }
        public String Arena { get; set; }
        public int MaxPlayers { get; set; }
        public String Ip { get; set; }
        public String GameMode { get; set; }
        public String FriendlyFire { get; set; }
        public int PingLimit { get; set; }
        public String DetailId { get; set; }
    }
}
