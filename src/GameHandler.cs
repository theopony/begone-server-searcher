﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using ExitGames.Client.Photon;
using ExitGames.Client.Photon.Lite;
using System.Collections;
using System.Threading;
using BeGone.ServerSearcher.Net;

namespace BeGone.ServerSearcher
{
    public class GameHandler : IPhotonPeerListener
    {
        public LitePeer Peer { get; private set; }
        public String IpPort { get; private set; }
        public String RoomName { get; private set; }
        public Thread Thread { get; private set; }
        public Boolean IsAlive { get; private set; }
        public Timer Timer { get; private set; }

        public const int MillisecondsToLive = 2000;

        private List<EventPlayerInfo> _players = new List<EventPlayerInfo>();

        public delegate void PlayersCallback(EventPlayerInfo[] players);
        public event PlayersCallback OnPlayers;

        public GameHandler(Room room)
        {
            this.IpPort = room.ServerInfo.Ip;
            this.RoomName = room.FullName;
        }

        /// <summary>
        /// Start the game handler.
        /// </summary>
        public void Start()
        {
            this.IsAlive = true;

            this.Thread = new Thread(this.Run);
            this.Thread.Start();

            this.Peer = new LitePeer(this, ConnectionProtocol.Udp);
            this.Peer.ChannelCount = 8;
            this.Peer.DebugOut = DebugLevel.OFF;

            if (!this.Peer.Connect(this.IpPort, "BeGoneServer"))
            {
                Console.WriteLine("Failed to connect : " + this.IpPort);
            }
        }

        private void StartTimer()
        {
            this.Timer = new Timer(this.HandleTimeout, null, MillisecondsToLive, Timeout.Infinite);
        }

        private void HandleTimeout(Object obj)
        {
            this.Timer.Dispose();
            this.Timer = null;

            Console.WriteLine("Timeout, Disconnecting");
            this.Peer.Disconnect();
            this.IsAlive = false;

            if (this.OnPlayers != null)
                this.OnPlayers(_players.ToArray());
        }

        /// <summary>
        /// You must use this after connecting to the server, to connect to the room you wish to join.
        /// </summary>
        /// <param name="roomName"></param>

        private void JoinWithLobby(string roomName)
        {
            Dictionary<byte, object> customOpParameters = new Dictionary<byte, object>();
            customOpParameters.Add(0xff, roomName);
            customOpParameters.Add(0xf2, "BeGone_lobby");
            this.Peer.OpCustom(0xff, customOpParameters, true);

            this.StartTimer();
        }

        public void DispatchNetworkEvent(PhotonEvent photonEvent, NetworkEvent e, bool isReliable, byte channel)
        {
            if ((this.Peer != null) && (e != null))
            {
                Hashtable info = e.GetInfo();
                this.Peer.OpRaiseEvent((byte)photonEvent, info, isReliable, channel);
            }
        }

        /// <summary>
        /// Photon event loop.
        /// </summary>
        public void Run()
        {
            while (this.IsAlive)
            {
                try
                {
                    this.Peer.Service();
                    Thread.Sleep(75);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Thread.Sleep(100);
                }
            }
        }

        public virtual void OnStatusChanged(StatusCode statusCode)
        {
            switch (statusCode)
            {
                case StatusCode.Connect:
                    Console.WriteLine("Joining the Room: " + this.RoomName);
                    this.JoinWithLobby(this.RoomName);
                    break;
                case StatusCode.ExceptionOnConnect:
                case StatusCode.Disconnect:
                case StatusCode.Exception:
                case StatusCode.TimeoutDisconnect:
                case StatusCode.DisconnectByServer:
                case StatusCode.DisconnectByServerUserLimit:
                case StatusCode.DisconnectByServerLogic:
                    Console.WriteLine("Unhandled Photon Error: " + statusCode);
                    break;

                default:
                    Console.WriteLine("Unhandled Photon Error: " + statusCode);
                    break;
            }
        }

        public void OnEvent(EventData photonEvent)
        {
            byte code = photonEvent.Code;
            int key = (int)photonEvent.Parameters[0xfe];

            //if (((key != 0) && (key != -1)) && (!PhotonHelper.GetPlayers().ContainsKey(key) && (code != 0xff)))
            //{
            //    Console.WriteLine(
            //        "Received an event from a player that doesnt exist! Player={0} Event={1}",
            //        key, photonEvent.ToStringFull());
            //}

            Hashtable info = new Hashtable();
            if (photonEvent.Parameters.ContainsKey(0xf5))
            {
                info = photonEvent.Parameters[0xf5] as Hashtable;
            }

            switch (code)
            {
                case (byte)PhotonEvent.PlayerInfo:
                    EventPlayerInfo e = new EventPlayerInfo(key, info);
                    _players.Add(e);
                    Console.WriteLine("EventPlayerInfo received: Id={0}, Name={1}", e.PlayerId, e.Name);
                    break;
            }
        }

        public void OnOperationResponse(OperationResponse or)
        {
        }

        public void DebugReturn(DebugLevel dl, String s)
        {
        }
    }
}
