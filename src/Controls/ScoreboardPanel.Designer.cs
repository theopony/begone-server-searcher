﻿namespace BeGone.ServerSearcher.Controls
{
    partial class ScoreboardPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MainTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.MilitiaTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.SwatTitleLabel = new System.Windows.Forms.Label();
            this.MilitiaTitleLabel = new System.Windows.Forms.Label();
            this.SwatTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.MainTableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainTableLayoutPanel
            // 
            this.MainTableLayoutPanel.ColumnCount = 2;
            this.MainTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.MainTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.MainTableLayoutPanel.Controls.Add(this.MilitiaTableLayoutPanel, 1, 1);
            this.MainTableLayoutPanel.Controls.Add(this.SwatTitleLabel, 0, 0);
            this.MainTableLayoutPanel.Controls.Add(this.MilitiaTitleLabel, 1, 0);
            this.MainTableLayoutPanel.Controls.Add(this.SwatTableLayoutPanel, 0, 1);
            this.MainTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.MainTableLayoutPanel.Name = "MainTableLayoutPanel";
            this.MainTableLayoutPanel.RowCount = 2;
            this.MainTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.MainTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.MainTableLayoutPanel.Size = new System.Drawing.Size(327, 287);
            this.MainTableLayoutPanel.TabIndex = 0;
            // 
            // MilitiaTableLayoutPanel
            // 
            this.MilitiaTableLayoutPanel.ColumnCount = 1;
            this.MilitiaTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.MilitiaTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MilitiaTableLayoutPanel.Location = new System.Drawing.Point(166, 23);
            this.MilitiaTableLayoutPanel.Name = "MilitiaTableLayoutPanel";
            this.MilitiaTableLayoutPanel.RowCount = 1;
            this.MilitiaTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.MilitiaTableLayoutPanel.Size = new System.Drawing.Size(158, 261);
            this.MilitiaTableLayoutPanel.TabIndex = 3;
            // 
            // SwatTitleLabel
            // 
            this.SwatTitleLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SwatTitleLabel.Location = new System.Drawing.Point(3, 0);
            this.SwatTitleLabel.Name = "SwatTitleLabel";
            this.SwatTitleLabel.Size = new System.Drawing.Size(157, 20);
            this.SwatTitleLabel.TabIndex = 0;
            this.SwatTitleLabel.Text = "SWAT";
            this.SwatTitleLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // MilitiaTitleLabel
            // 
            this.MilitiaTitleLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MilitiaTitleLabel.Location = new System.Drawing.Point(166, 0);
            this.MilitiaTitleLabel.Name = "MilitiaTitleLabel";
            this.MilitiaTitleLabel.Size = new System.Drawing.Size(158, 20);
            this.MilitiaTitleLabel.TabIndex = 1;
            this.MilitiaTitleLabel.Text = "Militia";
            this.MilitiaTitleLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // SwatTableLayoutPanel
            // 
            this.SwatTableLayoutPanel.ColumnCount = 1;
            this.SwatTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.SwatTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SwatTableLayoutPanel.Location = new System.Drawing.Point(3, 23);
            this.SwatTableLayoutPanel.Name = "SwatTableLayoutPanel";
            this.SwatTableLayoutPanel.RowCount = 1;
            this.SwatTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.SwatTableLayoutPanel.Size = new System.Drawing.Size(157, 261);
            this.SwatTableLayoutPanel.TabIndex = 2;
            // 
            // ScoreboardPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.MainTableLayoutPanel);
            this.Name = "ScoreboardPanel";
            this.Size = new System.Drawing.Size(327, 287);
            this.MainTableLayoutPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel MainTableLayoutPanel;
        private System.Windows.Forms.Label SwatTitleLabel;
        private System.Windows.Forms.Label MilitiaTitleLabel;
        private System.Windows.Forms.TableLayoutPanel SwatTableLayoutPanel;
        private System.Windows.Forms.TableLayoutPanel MilitiaTableLayoutPanel;
    }
}
