﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BeGone.ServerSearcher.Controls
{
    public partial class ScoreboardEntryPanel : UserControl
    {
        public EventPlayerInfo Player { get; private set; }

        public ScoreboardEntryPanel(EventPlayerInfo player)
        {
            InitializeComponent();

            this.Player = player;
            this.NameLabel.Text = player.Name;
            this.ScoreLabel.Text = player.Kills.ToString() + " / " + player.Deaths.ToString();
        }
    }
}
