﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace BeGone.ServerSearcher.Controls
{
    public partial class MapSelectionPanel : UserControl
    {
        public struct State
        {
            public Boolean Warehouse;
            public Boolean Courtyard;
            public Boolean Tower;
            public Boolean Timbertown;
            public Boolean Crane;
            public Boolean Pipeline;
        }

        public delegate void StateChangedCallback(State state);
        public event StateChangedCallback OnStateChanged;

        private Object _eventLock = new Object();

        public CheckBox[] CheckBoxes
        {
            get
            {
                return new CheckBox[]
                {
                    this.WarehouseCheckBox,
                    this.CourtyardCheckBox,
                    this.TowerCheckBox,
                    this.TimbertownCheckBox,
                    this.CraneCheckBox,
                    this.PipelineCheckBox
                };
            }
        }

        /// <summary>
        /// Whether or not several values are changing simultaneously, such as if a "Select/Unselect All"
        /// button is clicked. This will cause no OnStateChanged events to be fired until all values have
        /// been updated.
        /// </summary>
        private Boolean _isChangingMany = false;

        public MapSelectionPanel()
        {
            InitializeComponent();
        }

        public State GenerateState()
        {
            State state = new State();
            state.Warehouse = this.WarehouseCheckBox.Checked;
            state.Courtyard = this.CourtyardCheckBox.Checked;
            state.Tower = this.TowerCheckBox.Checked;
            state.Timbertown = this.TimbertownCheckBox.Checked;
            state.Crane = this.CraneCheckBox.Checked;
            state.Pipeline = this.PipelineCheckBox.Checked;
            return state;
        }

        private void UpdateAllChecked(Boolean isChecked)
        {
            lock (_eventLock)
            {
                _isChangingMany = true;
                foreach (CheckBox checkBox in this.CheckBoxes)
                    checkBox.Checked = isChecked;
                _isChangingMany = false;

                if (this.OnStateChanged != null)
                    this.OnStateChanged(this.GenerateState());
            }
        }

        private void CheckBox_CheckedChanged(object sender, EventArgs e)
        {
            lock (_eventLock)
            {
                State state = this.GenerateState();
                if (this.OnStateChanged != null && !_isChangingMany)
                    this.OnStateChanged(state);
            }
        }

        private void SelectAllButton_Click(object sender, EventArgs e)
        {
            this.UpdateAllChecked(true);
        }

        private void UnselectAllButton_Click(object sender, EventArgs e)
        {
            this.UpdateAllChecked(false);
        }
    }
}
