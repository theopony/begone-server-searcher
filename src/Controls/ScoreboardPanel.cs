﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BeGone.ServerSearcher.Controls
{
    public partial class ScoreboardPanel : UserControl
    {
        /// <summary>
        /// Scoreboard row height in pixels.
        /// </summary>
        public const int RowHeight = 30;

        private int _swatIndex = 0;
        private int _militiaIndex = 0;

        public ScoreboardPanel()
        {
            InitializeComponent();
            InitializeTableLayoutPanel(this.SwatTableLayoutPanel);
            InitializeTableLayoutPanel(this.MilitiaTableLayoutPanel);
        }

        private void InitializeTableLayoutPanel(TableLayoutPanel layout)
        {
            this.SuspendLayout();
            layout.SuspendLayout();

            // Remove the required initial row
            layout.RowStyles.RemoveAt(0);

            // Unsure of a better (dynamic) way to do this
            layout.RowCount = 24;
            //this.SwatTableLayoutPanel.Controls.RemoveAt(0); // Remove forced initial row
            for (int i = 0; i < layout.RowCount; i++)
                layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));

            layout.ResumeLayout();
            this.ResumeLayout();
        }

        /// <summary>
        /// Comparison method to sort players by kills and deaths.
        /// </summary>
        /// <param name="p1">Player</param>
        /// <param name="p2">Player</param>
        /// <returns></returns>
        private static int SortPlayers(EventPlayerInfo p1, EventPlayerInfo p2)
        {
            int k = p2.Kills - p1.Kills;
            if (k != 0) return k;

            int d = p1.Deaths - p2.Deaths;
            return d;
        }

        public void Update(Room room)
        {
            List<EventPlayerInfo> players = new List<EventPlayerInfo>(room.Players);
            players.Sort(SortPlayers);

            this.SuspendLayout();

            foreach(EventPlayerInfo player in players)
            {
                this.AddPlayer(player);
            }

            this.ResumeLayout();
        }

        /// <summary>
        /// Add a player to the scoreboard.
        /// </summary>
        /// <param name="player">Player to add</param>
        private void AddPlayer(EventPlayerInfo player)
        {
            ScoreboardEntryPanel entry = new ScoreboardEntryPanel(player);
            entry.Height = ScoreboardPanel.RowHeight;

            if(player.TeamId == 0)
            {
                //this.SwatTableLayoutPanel.RowCount += 1;
                //this.SwatTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
                this.SwatTableLayoutPanel.Controls.Add(entry, 0, _swatIndex);
                _swatIndex++;
            }
            else if(player.TeamId == 1)
            {
                //this.MilitiaTableLayoutPanel.RowCount += 1;
                //this.MilitiaTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
                this.MilitiaTableLayoutPanel.Controls.Add(entry, 1, _militiaIndex);
                _militiaIndex++;
            }
        }
    }
}
