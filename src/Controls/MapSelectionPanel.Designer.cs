﻿namespace BeGone.ServerSearcher.Controls
{
    partial class MapSelectionPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MapsGroupBox = new System.Windows.Forms.GroupBox();
            this.MapsLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.CourtyardPanel = new System.Windows.Forms.Panel();
            this.CourtyardCheckBox = new System.Windows.Forms.CheckBox();
            this.WarehousePanel = new System.Windows.Forms.Panel();
            this.WarehouseCheckBox = new System.Windows.Forms.CheckBox();
            this.TimbertownPanel = new System.Windows.Forms.Panel();
            this.TimbertownCheckBox = new System.Windows.Forms.CheckBox();
            this.CranePanel = new System.Windows.Forms.Panel();
            this.CraneCheckBox = new System.Windows.Forms.CheckBox();
            this.TowerPanel = new System.Windows.Forms.Panel();
            this.TowerCheckBox = new System.Windows.Forms.CheckBox();
            this.PipelinePanel = new System.Windows.Forms.Panel();
            this.PipelineCheckBox = new System.Windows.Forms.CheckBox();
            this.BottomPanel = new System.Windows.Forms.Panel();
            this.UnselectAllButton = new System.Windows.Forms.Button();
            this.SelectAllButton = new System.Windows.Forms.Button();
            this.MapsGroupBox.SuspendLayout();
            this.MapsLayoutPanel.SuspendLayout();
            this.CourtyardPanel.SuspendLayout();
            this.WarehousePanel.SuspendLayout();
            this.TimbertownPanel.SuspendLayout();
            this.CranePanel.SuspendLayout();
            this.TowerPanel.SuspendLayout();
            this.PipelinePanel.SuspendLayout();
            this.BottomPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // MapsGroupBox
            // 
            this.MapsGroupBox.Controls.Add(this.MapsLayoutPanel);
            this.MapsGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MapsGroupBox.Location = new System.Drawing.Point(0, 0);
            this.MapsGroupBox.Name = "MapsGroupBox";
            this.MapsGroupBox.Padding = new System.Windows.Forms.Padding(6);
            this.MapsGroupBox.Size = new System.Drawing.Size(343, 105);
            this.MapsGroupBox.TabIndex = 0;
            this.MapsGroupBox.TabStop = false;
            this.MapsGroupBox.Text = "Maps";
            // 
            // MapsLayoutPanel
            // 
            this.MapsLayoutPanel.ColumnCount = 3;
            this.MapsLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33F));
            this.MapsLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33F));
            this.MapsLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.34F));
            this.MapsLayoutPanel.Controls.Add(this.CourtyardPanel, 1, 0);
            this.MapsLayoutPanel.Controls.Add(this.WarehousePanel, 0, 0);
            this.MapsLayoutPanel.Controls.Add(this.TimbertownPanel, 0, 1);
            this.MapsLayoutPanel.Controls.Add(this.CranePanel, 1, 1);
            this.MapsLayoutPanel.Controls.Add(this.TowerPanel, 2, 0);
            this.MapsLayoutPanel.Controls.Add(this.PipelinePanel, 2, 1);
            this.MapsLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MapsLayoutPanel.Location = new System.Drawing.Point(6, 19);
            this.MapsLayoutPanel.Name = "MapsLayoutPanel";
            this.MapsLayoutPanel.RowCount = 2;
            this.MapsLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.MapsLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.MapsLayoutPanel.Size = new System.Drawing.Size(331, 80);
            this.MapsLayoutPanel.TabIndex = 0;
            // 
            // CourtyardPanel
            // 
            this.CourtyardPanel.Controls.Add(this.CourtyardCheckBox);
            this.CourtyardPanel.Location = new System.Drawing.Point(113, 3);
            this.CourtyardPanel.Name = "CourtyardPanel";
            this.CourtyardPanel.Size = new System.Drawing.Size(104, 34);
            this.CourtyardPanel.TabIndex = 0;
            // 
            // CourtyardCheckBox
            // 
            this.CourtyardCheckBox.AutoSize = true;
            this.CourtyardCheckBox.Checked = true;
            this.CourtyardCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CourtyardCheckBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CourtyardCheckBox.Location = new System.Drawing.Point(0, 0);
            this.CourtyardCheckBox.Name = "CourtyardCheckBox";
            this.CourtyardCheckBox.Size = new System.Drawing.Size(104, 34);
            this.CourtyardCheckBox.TabIndex = 0;
            this.CourtyardCheckBox.Text = "Courtyard";
            this.CourtyardCheckBox.UseVisualStyleBackColor = true;
            this.CourtyardCheckBox.CheckedChanged += new System.EventHandler(this.CheckBox_CheckedChanged);
            // 
            // WarehousePanel
            // 
            this.WarehousePanel.Controls.Add(this.WarehouseCheckBox);
            this.WarehousePanel.Location = new System.Drawing.Point(3, 3);
            this.WarehousePanel.Name = "WarehousePanel";
            this.WarehousePanel.Size = new System.Drawing.Size(104, 34);
            this.WarehousePanel.TabIndex = 1;
            // 
            // WarehouseCheckBox
            // 
            this.WarehouseCheckBox.AutoSize = true;
            this.WarehouseCheckBox.Checked = true;
            this.WarehouseCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.WarehouseCheckBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.WarehouseCheckBox.Location = new System.Drawing.Point(0, 0);
            this.WarehouseCheckBox.Name = "WarehouseCheckBox";
            this.WarehouseCheckBox.Size = new System.Drawing.Size(104, 34);
            this.WarehouseCheckBox.TabIndex = 0;
            this.WarehouseCheckBox.Text = "Warehouse";
            this.WarehouseCheckBox.UseVisualStyleBackColor = true;
            this.WarehouseCheckBox.CheckedChanged += new System.EventHandler(this.CheckBox_CheckedChanged);
            // 
            // TimbertownPanel
            // 
            this.TimbertownPanel.Controls.Add(this.TimbertownCheckBox);
            this.TimbertownPanel.Location = new System.Drawing.Point(3, 43);
            this.TimbertownPanel.Name = "TimbertownPanel";
            this.TimbertownPanel.Size = new System.Drawing.Size(104, 34);
            this.TimbertownPanel.TabIndex = 2;
            // 
            // TimbertownCheckBox
            // 
            this.TimbertownCheckBox.AutoSize = true;
            this.TimbertownCheckBox.Checked = true;
            this.TimbertownCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.TimbertownCheckBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TimbertownCheckBox.Location = new System.Drawing.Point(0, 0);
            this.TimbertownCheckBox.Name = "TimbertownCheckBox";
            this.TimbertownCheckBox.Size = new System.Drawing.Size(104, 34);
            this.TimbertownCheckBox.TabIndex = 0;
            this.TimbertownCheckBox.Text = "Timbertown";
            this.TimbertownCheckBox.UseVisualStyleBackColor = true;
            this.TimbertownCheckBox.CheckedChanged += new System.EventHandler(this.CheckBox_CheckedChanged);
            // 
            // CranePanel
            // 
            this.CranePanel.Controls.Add(this.CraneCheckBox);
            this.CranePanel.Location = new System.Drawing.Point(113, 43);
            this.CranePanel.Name = "CranePanel";
            this.CranePanel.Size = new System.Drawing.Size(104, 34);
            this.CranePanel.TabIndex = 3;
            // 
            // CraneCheckBox
            // 
            this.CraneCheckBox.AutoSize = true;
            this.CraneCheckBox.Checked = true;
            this.CraneCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CraneCheckBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CraneCheckBox.Location = new System.Drawing.Point(0, 0);
            this.CraneCheckBox.Name = "CraneCheckBox";
            this.CraneCheckBox.Size = new System.Drawing.Size(104, 34);
            this.CraneCheckBox.TabIndex = 0;
            this.CraneCheckBox.Text = "Crane";
            this.CraneCheckBox.UseVisualStyleBackColor = true;
            this.CraneCheckBox.CheckedChanged += new System.EventHandler(this.CheckBox_CheckedChanged);
            // 
            // TowerPanel
            // 
            this.TowerPanel.Controls.Add(this.TowerCheckBox);
            this.TowerPanel.Location = new System.Drawing.Point(223, 3);
            this.TowerPanel.Name = "TowerPanel";
            this.TowerPanel.Size = new System.Drawing.Size(105, 34);
            this.TowerPanel.TabIndex = 4;
            // 
            // TowerCheckBox
            // 
            this.TowerCheckBox.AutoSize = true;
            this.TowerCheckBox.Checked = true;
            this.TowerCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.TowerCheckBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TowerCheckBox.Location = new System.Drawing.Point(0, 0);
            this.TowerCheckBox.Name = "TowerCheckBox";
            this.TowerCheckBox.Size = new System.Drawing.Size(105, 34);
            this.TowerCheckBox.TabIndex = 0;
            this.TowerCheckBox.Text = "Tower";
            this.TowerCheckBox.UseVisualStyleBackColor = true;
            this.TowerCheckBox.CheckedChanged += new System.EventHandler(this.CheckBox_CheckedChanged);
            // 
            // PipelinePanel
            // 
            this.PipelinePanel.Controls.Add(this.PipelineCheckBox);
            this.PipelinePanel.Location = new System.Drawing.Point(223, 43);
            this.PipelinePanel.Name = "PipelinePanel";
            this.PipelinePanel.Size = new System.Drawing.Size(105, 34);
            this.PipelinePanel.TabIndex = 5;
            // 
            // PipelineCheckBox
            // 
            this.PipelineCheckBox.AutoSize = true;
            this.PipelineCheckBox.Checked = true;
            this.PipelineCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.PipelineCheckBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PipelineCheckBox.Location = new System.Drawing.Point(0, 0);
            this.PipelineCheckBox.Name = "PipelineCheckBox";
            this.PipelineCheckBox.Size = new System.Drawing.Size(105, 34);
            this.PipelineCheckBox.TabIndex = 0;
            this.PipelineCheckBox.Text = "Pipeline";
            this.PipelineCheckBox.UseVisualStyleBackColor = true;
            this.PipelineCheckBox.CheckedChanged += new System.EventHandler(this.CheckBox_CheckedChanged);
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.UnselectAllButton);
            this.BottomPanel.Controls.Add(this.SelectAllButton);
            this.BottomPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomPanel.Location = new System.Drawing.Point(0, 105);
            this.BottomPanel.Name = "BottomPanel";
            this.BottomPanel.Padding = new System.Windows.Forms.Padding(3);
            this.BottomPanel.Size = new System.Drawing.Size(343, 31);
            this.BottomPanel.TabIndex = 1;
            // 
            // UnselectAllButton
            // 
            this.UnselectAllButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.UnselectAllButton.Location = new System.Drawing.Point(190, 3);
            this.UnselectAllButton.Name = "UnselectAllButton";
            this.UnselectAllButton.Size = new System.Drawing.Size(75, 25);
            this.UnselectAllButton.TabIndex = 1;
            this.UnselectAllButton.Text = "Unselect All";
            this.UnselectAllButton.UseVisualStyleBackColor = true;
            this.UnselectAllButton.Click += new System.EventHandler(this.UnselectAllButton_Click);
            // 
            // SelectAllButton
            // 
            this.SelectAllButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.SelectAllButton.Location = new System.Drawing.Point(265, 3);
            this.SelectAllButton.Name = "SelectAllButton";
            this.SelectAllButton.Size = new System.Drawing.Size(75, 25);
            this.SelectAllButton.TabIndex = 0;
            this.SelectAllButton.Text = "Select All";
            this.SelectAllButton.UseVisualStyleBackColor = true;
            this.SelectAllButton.Click += new System.EventHandler(this.SelectAllButton_Click);
            // 
            // MapSelectionPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.MapsGroupBox);
            this.Controls.Add(this.BottomPanel);
            this.Name = "MapSelectionPanel";
            this.Size = new System.Drawing.Size(343, 136);
            this.MapsGroupBox.ResumeLayout(false);
            this.MapsLayoutPanel.ResumeLayout(false);
            this.CourtyardPanel.ResumeLayout(false);
            this.CourtyardPanel.PerformLayout();
            this.WarehousePanel.ResumeLayout(false);
            this.WarehousePanel.PerformLayout();
            this.TimbertownPanel.ResumeLayout(false);
            this.TimbertownPanel.PerformLayout();
            this.CranePanel.ResumeLayout(false);
            this.CranePanel.PerformLayout();
            this.TowerPanel.ResumeLayout(false);
            this.TowerPanel.PerformLayout();
            this.PipelinePanel.ResumeLayout(false);
            this.PipelinePanel.PerformLayout();
            this.BottomPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox MapsGroupBox;
        private System.Windows.Forms.Panel BottomPanel;
        private System.Windows.Forms.TableLayoutPanel MapsLayoutPanel;
        private System.Windows.Forms.Panel WarehousePanel;
        private System.Windows.Forms.Panel TimbertownPanel;
        private System.Windows.Forms.Panel CranePanel;
        private System.Windows.Forms.Panel TowerPanel;
        private System.Windows.Forms.Panel PipelinePanel;
        private System.Windows.Forms.Panel CourtyardPanel;
        private System.Windows.Forms.CheckBox WarehouseCheckBox;
        private System.Windows.Forms.CheckBox CourtyardCheckBox;
        private System.Windows.Forms.CheckBox TimbertownCheckBox;
        private System.Windows.Forms.CheckBox TowerCheckBox;
        private System.Windows.Forms.CheckBox CraneCheckBox;
        private System.Windows.Forms.CheckBox PipelineCheckBox;
        private System.Windows.Forms.Button SelectAllButton;
        private System.Windows.Forms.Button UnselectAllButton;
    }
}
